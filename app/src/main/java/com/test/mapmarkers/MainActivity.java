package com.test.mapmarkers;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.Sprite;
import com.mapbox.mapboxsdk.annotations.SpriteFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.views.MapView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Marker marker;
    private MapView map;
    private Sprite sprite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        map = (MapView) findViewById(R.id.map);
        map.setCenterCoordinate(new LatLng(37.76, -122.42));
        map.setStyleUrl(Style.LIGHT);
        map.setZoomLevel(11);
        map.onCreate(savedInstanceState);

        SpriteFactory spriteFactory = new SpriteFactory(map);
        Drawable icon = ContextCompat.getDrawable(getApplicationContext(), R.drawable.marker_green);
        sprite = spriteFactory.fromDrawable(icon);

        drawMarker(null);

        findViewById(R.id.hideMarker).setOnClickListener(this);
        findViewById(R.id.showMarker).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hideMarker:
                if (marker != null)
                    marker.remove();
                break;
            case R.id.showMarker:
                drawMarker(sprite);
                break;
        }
    }

    private void drawMarker(Sprite sprite) {
        MarkerOptions mo = new MarkerOptions();
        mo.position(new LatLng(37.76, -122.42));

        if (sprite != null)
            mo.icon(sprite);

        if (map != null)
            marker = map.addMarker(mo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        map.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        map.onStop();
    }

    @Override
    public void onPause()  {
        super.onPause();
        map.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        map.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        map.onSaveInstanceState(outState);
    }

}
